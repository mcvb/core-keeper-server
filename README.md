# Core Keeper Server

Core Keeper server deployment.

Get [Core Keeper](https://store.steampowered.com/app/1621690/Core_Keeper/) game.

This configurations are inspired by :

* [core-keeper-dedicated](https://github.com/escapingnetwork/core-keeper-dedicated) Docker image.

* [This topic from Grubhead](https://steamcommunity.com/app/1621690/discussions/0/3275817873363439916/).

## Prerequisites

- [Ubuntu server](https://ubuntu.com/download/server?version=20.04&architecture=amd64) == 22.04

- [SteamCMD](https://doc.ubuntu-fr.org/steamcmd)

## Manual installation

### Packages

Install `steamcmd` :

```bash
# Install software-properties-common
sudo apt update && sudo apt install software-properties-common

# Enable multiverse repository
sudo add-apt-repository multiverse

# Enable i386 architecture
sudo dpkg --add-architecture i386

# Install steamcmd and xvfb
sudo apt update && sudo apt install steamcmd xvfb
```

### User

Create `steam` user :

```bash
# Create steam system user
sudo useradd -r -U -m -d /home/steam -s /bin/bash steam
sudo chmod 0700 /home/steam
```

Create `/srv/steam` directory :

```bash
sudo mkdir /srv/corekeeper
sudo chown -R steam:steam /srv/corekeeper
sudo chmod -R 0700 /srv/corekeeper
```

### Install Core Keeper server

Log as `steam` :

```bash
sudo su steam
```

Install Core Keeper with `steamcmd` :

```bash
export STEAM_APPID=1007
export STEAM_APPID_TOOL=1963720

steamcmd +login anonymous +force_install_dir /srv/corekeeper +app_update "$STEAM_APPID" +app_update "$STEAM_APPID_TOOL" +quit
```

Run Core Keeper server (see [this section](#Core-Keeper-parameters) for launch parameters) :

```bash
# Start server
/bin/bash /srv/corekeeper/_launch.sh -datapath /srv/corekeeper/saves

# Start with parameters
/bin/bash /srv/corekeeper/_launch.sh -world 0 -worldname "Server" -worldseed 0 -gameid "" -datapath "" -maxplayers 5 -worldmode 0 -port <unset> -ip 0.0.0.0
```

### Create custom launch script

Create a custom launch script `/usr/local/bin/launch-corekeeper` :

```bash
sudo touch /usr/local/bin/launch-corekeeper
sudo chown steam:steam /usr/local/bin/launch-corekeeper
sudo chmod 0700 /usr/local/bin/launch-corekeeper
```

Paste the content of [launch-corekeeper.sh](files/launch-corekeeper.sh) into `/usr/local/bin/launch-corekeeper`.

### Configure Systemd service

Create systemd service file :

```bash
# Create service file
sudo touch /etc/systemd/system/corekeeper.service
sudo chown root:root /etc/systemd/system/corekeeper.service
sudo chmod 0600 /etc/systemd/system/corekeeper.service
```

Edit `/etc/systemd/system/corekeeper.service` and add this content :

```text
[Install]
WantedBy=multi-user.target

[Unit]
Description=Core Keeper Service
After=network.target

[Service]
Type=simple
User=steam
Group=steam
KillMode=control-group
SuccessExitStatus=0 1
ProtectHome=true
ProtectSystem=full
PrivateDevices=true
NoNewPrivileges=true
WorkingDirectory=/srv/corekeeper
ReadWritePaths=/srv/corekeeper
ReadWritePaths=/home/steam/Steam
ExecStart=/usr/local/bin/launch-corekeeper /srv/corekeeper/saves
Restart=always

[Install]
WantedBy=multi-user.target
```

Start service :

```bash
sudo systemctl daemon-reload && sudo systemctl start corekeeper.service
```

Check service status :

```bash
sudo systemctl status corekeeper.service
```

Enable service at startup :

```bash
sudo systemctl enable corekeeper.service
```

## Ansible installation

Deploy Core Keeper server with Ansible ([ansible-playbook](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html)).

* Configure `corekeeperservers` in your Ansible inventory.

* Copy and edit variables file [corekeeper.yml](vars/corekeeper.yml) :

```bash
cp vars/corekeeper.yml{,.example}

vim vars/corekeeper.yml
```

* Run Ansible playbook :

```bash
# Check hosts
ansible-playbook corekeeper.yml --list-host

# Run playbook
ansible-playbook corekeeper.yml
```

## Core Keeper parameters

From Core Keeper Readme :

| Parameter     | Example                | Description                                                                                                                                                               |
| ------------- | ---------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `-world`      | `0`                    | Which world index to use.                                                                                                                                                 |
| `-worldname`  | `"Core Keeper Server"` | The name to use for the server.                                                                                                                                           |
| `-worldseed`  | `0`                    | The seed to use for a new world. Set to 0 to generate random seed.                                                                                                        |
| `-gameid`     | `"uatv381JBghTrp"`     | Game ID to use for the server. Need to be at least 23 characters and alphanumeric, excluding Y,y,x,0,O. Empty or not valid means a new ID will be generated at start.     |
| `-datapath`   | `/srv/corekeeper/data` | Save file location. If not set it defaults to a subfolder named "DedicatedServer" at the default Core Keeper save location.                                               |
| `-maxplayers` | `5`                    | Maximum number of players that will be allowed to connect to server.                                                                                                      |
| `-worldmode`  | `0`                    | Whether to use normal (0) or hard (1) mode for world.                                                                                                                     |
| `-port`       | `<unset>`              | What port to bind to. If not set, then the server will use the Steam relay network. If set the clients will connect to the server directly and the port needs to be open. |
| `-ip`         | `0.0.0.0`              | Only used if port is set. Sets the address that the server will bind to.                                                                                                  |

## Updating

Stop Core Keeper service :

```bash
sudo systemctl stop corekeeper.service
```

Update Core Keeper with `steamcmd` :

```bash
# Use the same command as for the installation of the server files
export STEAM_APPID=1007
export STEAM_APPID_TOOL=1963720

steamcmd +login anonymous +force_install_dir /srv/corekeeper +app_update "$STEAM_APPID" +app_update "$STEAM_APPID_TOOL" +quit
```

Restart Core Keeper service :

```bash
sudo systemctl start corekeeper.service
```
