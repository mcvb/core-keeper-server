#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Backup Core Keeper server

import os
import re
import sys
import time
import logging
import tarfile
import subprocess
from datetime import datetime
from argparse import ArgumentParser

INFOFORMATTER = "%(asctime)s [%(levelname)s] : %(message)s"
DATEFORMATTER = "%Y-%m-%d %H:%M:%S"


class StdoutFilter(logging.Filter):
    """ Filter log level for stdout handler
    """

    def filter(self, record):
        return record.levelno in (logging.DEBUG, logging.INFO)


logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt=INFOFORMATTER, datefmt=DATEFORMATTER)

handler_stdout = logging.StreamHandler(sys.stdout)
handler_stdout.addFilter(StdoutFilter())
handler_stdout.setLevel(logging.INFO)
handler_stdout.setFormatter(formatter)
logger.addHandler(handler_stdout)

handler_stderr = logging.StreamHandler(sys.stderr)
handler_stderr.setLevel(logging.WARNING)
handler_stderr.setFormatter(formatter)
logger.addHandler(handler_stderr)


def check_directory(dirname: str):
    """ Checks if a directory exists and is writable

    Parameters:
        directory (str): directory to check

    Returns:
        exists (bool): True if the directory exists
    """
    try:
        if not os.path.isdir(dirname):
            raise Exception(f"no such directory {dirname}")
        # Check if directory is writable
        if not os.access(dirname, os.W_OK):
            raise Exception(f"directory {dirname} is not writable")
    except Exception as e:
        logging.critical(e)
        sys.exit(1)
    else:
        return True


def tar(dirname: str, archive_name: str):
    """Create a compressed tarball from a directory

    Parameters:
        dirname (str): directory to tar
        archive_name (str): output archive name
    """
    try:
        with tarfile.open(archive_name, "w:gz") as tb:
            for root, _, files in os.walk(dirname):
                for file in files:
                    tb.add(os.path.join(root, file))
    except Exception as e:
        logging.critical(e)
        sys.exit(1)


def prune_old_archives(days: int, dirname: str, file_pattern: str):
    """Remove archives older than the specified number of days

    Parameters:
        days (int): remove all archives older than this number of days
        dirname (str): archives directory
        file_pattern (str): search files from pattern

    Returns:
        deleted (list): list of deleted archives
    """
    # 86400 = number of seconds per days
    search_time = time.time() - (days * 86400)
    deleted = []

    try:
        for archive in os.listdir(dirname):
            if re.match(file_pattern, archive):
                path = os.path.join(dirname, archive)
                if os.stat(path).st_mtime <= search_time:
                    deleted.append(path)
                    os.remove(path)
                    logging.info(f"file {path} has been deleted")
    except Exception as e:
        logging.critical(e)
    else:
        return deleted


def parse_args():
    """Configure argument parser"""
    description = ("Backup Core Keeper server")
    parser = ArgumentParser(description=description)
    parser.add_argument("-D", "--debug", help="Display debugging information",
                        action="store_const", dest="LOG_LEVEL",
                        const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument("-d", "--backups-directory", required=True,
                        help="Where to save the backup archives")
    parser.add_argument("-t", "--corekeeper-directory", required=True,
                        help="Location of corekeeper server files")
    parser.add_argument("-r", "--retention-days", type=int, default=7,
                        help="Number of days backup archives are kept")

    args = parser.parse_args()

    # Change log level according to -d parameter
    logger.setLevel(args.LOG_LEVEL)
    handler_stdout.setLevel(args.LOG_LEVEL)

    return args


def main():
    args = parse_args()
    backups_directory = args.backups_directory
    corekeeper_directory = args.corekeeper_directory
    retention_days = args.retention_days

    # Check directories
    check_directory(backups_directory)
    check_directory(corekeeper_directory)

    logging.info("backup Core Keeper server...")

    # Set backup archive name and path
    date = datetime.now().strftime("%Y%m%d")
    prefix = "backup_corekeeper"
    backup_name = os.path.join(backups_directory, f"{prefix}_{date}.tgz")

    logging.info("creating backup archives...")
    tar(dirname=corekeeper_directory, archive_name=backup_name)

    # Prune old archives (older than retention_days)
    logging.info(f"pruning archives older than {retention_days} days")
    archive_pattern = fr"{prefix}_.*\.tgz"
    prune_old_archives(days=retention_days, dirname=backups_directory,
                       file_pattern=archive_pattern)

    logging.info("Core Keeper server successfully backed up !")


if __name__ == "__main__":
    main()
