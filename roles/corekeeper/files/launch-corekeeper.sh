#!/usr/bin/env bash

xvfbpid=""
ckpid=""

SERVER_DIR="${1:?Usage : launch-corekeeper SERVER_DIR}"
readonly exepath="$SERVER_DIR/CoreKeeperServer"
readonly controlfile="$SERVER_DIR/GameID.txt"

function kill_corekeeperserver {
    if [[ -n "$ckpid" ]]; then
        kill "$ckpid"
        wait "$ckpid"
    fi
    if [[ -n "$xvfbpid" ]]; then
        kill "$xvfbpid"
        wait "$xvfbpid"
    fi
}

trap kill_corekeeperserver EXIT

# Enable job control
set -m

rm -f /tmp/.X99-lock

Xvfb :99 -screen 0 1x1x24 -nolisten tcp &
xvfbpid=$!

rm -f "$controlfile"

chmod +x "$exepath"

export DISPLAY=:99
export LD_LIBRARY_PATH="LD_LIBRARY_PATH:$SERVER_DIR/linux64"
"$exepath" -batchmode -datapath "$SERVER_DIR/saves" &
ckpid=$!

wait "$ckpid"
ckpid=""
